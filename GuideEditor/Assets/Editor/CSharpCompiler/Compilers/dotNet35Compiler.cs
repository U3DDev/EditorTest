﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Diagnostics;

public class dotNet35Compiler : BaseCompiler
{
    public const string dotNet35CompilerPath = @"C:\Windows\Microsoft.NET\Framework\v4.0.30319\csc.exe";

    public override void BuildCode (List<string> codefiles, string defines, string outPutPath,string filePath)
    {

        Process p = new Process();
        p.StartInfo.FileName = "cmd.exe";
        p.StartInfo.UseShellExecute = false;
        p.StartInfo.RedirectStandardInput = true;
        p.StartInfo.RedirectStandardOutput = true;
        p.StartInfo.RedirectStandardError = true;
        p.StartInfo.CreateNoWindow = true;
        System.Console.InputEncoding = System.Text.Encoding.UTF8;
        p.Start();
        StreamWriter sw = p.StandardInput;
        p.StandardInput.WriteLine("");
        p.StandardInput.WriteLine(Path.GetPathRoot(filePath));
        p.StandardInput.WriteLine("cd " + filePath);
        p.StandardInput.WriteLine(dotNet35CompilerPath + GetDefineString(defines) + " /out:" + outPutPath + " /warn:0" + " /target:library " + "*.cs"); 
        sw.Close();
        string logText = p.StandardOutput.ReadToEnd();
        if (logText.Contains("error"))
        {
            UnityEngine.Debug.LogError(logText);
        }
        else
        {
            UnityEngine.Debug.Log(logText);
        }


    }
    public string GetDefineString(string defines)
    {
        if (!string.IsNullOrEmpty(defines))
        {
            return "/define:"+defines;
        }
        return string.Empty;
    }



}

