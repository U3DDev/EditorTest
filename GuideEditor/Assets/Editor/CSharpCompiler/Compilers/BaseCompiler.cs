﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public abstract class BaseCompiler 
{


    public abstract void BuildCode (List<string> codefiles, string defines, string outPutPath,string filePath);


}

public class CompilerDefines
{
    public const string dotNet35Compiler ="dotNet35Compiler"; 

}