﻿using UnityEngine;
using System.Collections;

public static class CompilerFactory 
{
    public static BaseCompiler GetCompiler(string name)
    {
        switch (name) 
        {
            case CompilerDefines.dotNet35Compiler:
                Debug.Log("Get dotNet35 Compiler");
                return new dotNet35Compiler();
        }

        return null;
    }

}

