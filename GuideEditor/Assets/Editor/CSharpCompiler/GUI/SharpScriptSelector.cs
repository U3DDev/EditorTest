﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using System.IO;
[System.Serializable]
public class ScriptFile
{
    public bool isSelected = false;
    public string FileName;
    public string FilePath;
    public ScriptFile(string fileName, string filePath)
    {
        FileName = fileName;
        FilePath = filePath;
    }
}

public class SharpScriptSelector
{
    private Dictionary<string, ScriptFile> m_scriptsMap = new Dictionary<string, ScriptFile>();


    public void SearchScripts(string folder, bool isRecursive, List<string> ignoreFiles = null, string searchingPattern = "*.cs")
    {
        string[] searchedFiles = Directory.GetFiles(folder, searchingPattern, isRecursive ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly);
        m_scriptsMap.Clear();
        for (int i = 0; i < searchedFiles.Length; i++)
        {
            string filePath = searchedFiles[i];
            string fileName = Path.GetFileNameWithoutExtension(filePath);
            if (!m_scriptsMap.ContainsKey(fileName))
            {
                if (ignoreFiles != null)
                {
                    if (ignoreFiles.Contains(fileName))
                    {
                        UnityEngine.Debug.Log("跳过文件 :" + fileName);
                        continue;
                    }
                }
                UnityEngine.Debug.Log("检索到文件: " + filePath + " Name:" + fileName);
                m_scriptsMap.Add(fileName, new ScriptFile(fileName, filePath));
            }
        }
    }
    public void SelectScript(string scriptName)
    {

    }
    public void DeSelectScript(string scriptName)
    {

    }


    /// <summary>
    /// 返回所有选择的文件
    /// </summary>
    /// <returns></returns>
    public List<string> GetAllSelectedFiles()
    {
        List<string> pathList = new List<string>();
        foreach (var item in m_scriptsMap.Values)
        {
            if (!pathList.Contains(item.FilePath))
            {
                if (item.isSelected)
                {
                    pathList.Add(item.FilePath);
                }

            }
        }
        return pathList;
    }


    Vector2 m_scrollPositon;
    public void DrawElements()
    {
        m_scrollPositon = GUILayout.BeginScrollView(m_scrollPositon);
        foreach (var script in m_scriptsMap.Values)
            script.isSelected = EditorGUILayout.Toggle(new GUIContent(script.FileName, script.FilePath), script.isSelected);
        GUILayout.EndScrollView();
    }
}


