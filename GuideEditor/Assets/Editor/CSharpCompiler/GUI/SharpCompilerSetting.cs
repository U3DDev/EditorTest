﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;


/// <summary>
/// 用于C#设置编译器选项 TODO 分离数据层 并支持保存配置
/// </summary>
public class SharpCompilerSetting
{
    //---DATA---
    public bool isDebugMode = false;
    public string ScriptDefines = "";
    public string ExportPath = @"D:\";
    public string DllName = "test";
    //----------


    public void Draw()
    {
        isDebugMode = GUILayout.Toggle(isDebugMode, new GUIContent("是否是Debug版本的"));
        ScriptDefines = UnityEditor.EditorGUILayout.TextField(new GUIContent("定义的宏 用逗号分隔"), ScriptDefines);
        ExportPath = UnityEditor.EditorGUILayout.TextField(new GUIContent("导出的路径"), ExportPath);
        DllName = UnityEditor.EditorGUILayout.TextField(new GUIContent("Dll名称"), DllName);
    }
}

