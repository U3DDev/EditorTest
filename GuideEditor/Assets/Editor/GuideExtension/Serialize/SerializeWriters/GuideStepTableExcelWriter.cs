﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Guide.Editor.Serialize;
namespace Guide.Editor
{

    public class GuideStepTableRow
    {
        public int belongGuideId;
        public int stepIndex;

        public UnityEngine.Rect Rect;
        public string environmentPrefab;
        public string operationType;
        public string operationParam;
        public string maskType;
        public string dialogType;
        public string isTransparent;
        public string isBlockRaycast;
        public string TextModuleId;
        public string FrameType;
        public string SkipButtonType;
    }
    public class GuideStepTableExcelWriter : SerializeWriter
    {
        private List<TableHead> m_GuideStepTableHeadList = new List<TableHead>();
        private SerializeWriter m_GuideStepTableWriter = new SerializeWriter();

        private List<TableHead> m_GuideStepExtraTableHeadList = new List<TableHead>();
        private SerializeWriter m_GuideStepExtraTableWriter = new SerializeWriter();

        private bool m_hasAdded = false;
        private int m_rowCount = 0;
        public GuideStepTableExcelWriter()
        {


            //初始化Step表头
            m_GuideStepTableHeadList.Add(new TableHead("小步骤", "tag", "int", "primary", "tag"));
            m_GuideStepTableHeadList.Add(new TableHead("大步骤", "guideId", "int", "index", ""));
            m_GuideStepTableHeadList.Add(new TableHead("小步骤序号", "stepId", "int", "none", ""));
  
            //初始化StepExtra表头
            m_GuideStepExtraTableHeadList.Add(new TableHead("小步骤", "tag", "int", "primary", "tag"));
            m_GuideStepExtraTableHeadList.Add(new TableHead("环境预制Id", "envPrefabId", "string", "none", ""));
            m_GuideStepExtraTableHeadList.Add(new TableHead("类型", "type", "string", "none", ""));
            m_GuideStepExtraTableHeadList.Add(new TableHead("事件遮罩类型", "maskType", "int", "none", ""));
            m_GuideStepExtraTableHeadList.Add(new TableHead("遮罩是否透明", "isAlpha0", "int", "none", ""));
            m_GuideStepExtraTableHeadList.Add(new TableHead("遮罩是否拦截点击", "isRaycast", "int", "none", ""));
            m_GuideStepExtraTableHeadList.Add(new TableHead("参数", "param", "string", "none", ""));
            m_GuideStepExtraTableHeadList.Add(new TableHead("对话框类型", "dialogType", "int", "none", ""));
            m_GuideStepExtraTableHeadList.Add(new TableHead("对话框X", "dialogPosX", "double", "none", ""));
            m_GuideStepExtraTableHeadList.Add(new TableHead("对话框Y", "dialogPosY", "double", "none", ""));
            m_GuideStepExtraTableHeadList.Add(new TableHead("对话框Width", "dialogWidth", "double", "none", ""));
            m_GuideStepExtraTableHeadList.Add(new TableHead("对话框Height", "dialogHeight", "double", "none", ""));
            m_GuideStepExtraTableHeadList.Add(new TableHead("引导内容(TextModuleId)", "textModuleId", "string", "none", ""));
            m_GuideStepExtraTableHeadList.Add(new TableHead("指引框类型", "frameType", "int", "none", ""));
            m_GuideStepExtraTableHeadList.Add(new TableHead("跳过按钮类型", "skipSide", "int", "none", ""));

        }
        public void SetRowCount(int rowCouunt)
        {
            m_rowCount = rowCouunt;
        }
        public void AddTableHead()
        {
            if (m_hasAdded)
            {
                return;
            }
            m_GuideStepTableWriter.PrepareWrite("GuideStep.xlsx", m_rowCount,3);
            m_GuideStepExtraTableWriter.PrepareWrite("GuideStepExtra.xlsx", m_rowCount, 15);

            foreach (var head in m_GuideStepTableHeadList)
            {
                m_GuideStepTableWriter.AddTableHead(head);
            }
            foreach (var head in m_GuideStepExtraTableHeadList)
            {
                m_GuideStepExtraTableWriter.AddTableHead(head);
            }
            m_hasAdded = true;
        }
        public void AddRow(int index, GuideStepTableRow rowData)
        {
            if (null != rowData)
            {
                int stepTag = (rowData.belongGuideId * 1000 + index);
                m_GuideStepTableWriter.AddTableData(index, 
                    stepTag.ToString(), 
                    rowData.belongGuideId.ToString(),
                    rowData.stepIndex.ToString());

                m_GuideStepExtraTableWriter.AddTableData(index,
                    stepTag.ToString(),
                    rowData.environmentPrefab,
                    rowData.operationType,
                    rowData.maskType,
                    rowData.isTransparent,
                    rowData.isBlockRaycast,
                    rowData.operationParam,
                    rowData.dialogType,
                    rowData.Rect.x.ToString(),
                    rowData.Rect.y.ToString(),
                    rowData.Rect.width.ToString(),
                    rowData.Rect.height.ToString(),
                    rowData.TextModuleId,
                    rowData.FrameType,
                    rowData.SkipButtonType);

            }

        }

        public void SaveToExcel(string fullPath)
        {
            m_GuideStepTableWriter.WriteToExcel(fullPath+"/GuideStep.xlsx");
            m_GuideStepExtraTableWriter.WriteToExcel(fullPath + "/GuideStepExtra.xlsx");
        }

    }
}


