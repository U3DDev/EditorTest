﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace Guide.Editor
{
    public class GuideTableRowData
    {
        public string guideId;
        public string canSkip;
        public string keyStep;
        public string hasNextGuide;
        public string nextGuideId;
        public string nextGuideStepId;
        public string trustClient;
    }
    public class EasyGuideTableBuilder : EasyTableBulder
    {
        public override string Commnet
        {
            get
            {
                return "大步骤,是否可以跳过,关键步骤,是否有引导接续,接续的引导大步骤,接续的引导小步骤,是否信任客户端";
            }
        }

        public override string FieldsNames
        {
            get
            {

                return "guideId,canSkip,keyStep,hasNextGuide,nextGuideId,nextGuideStepId,trustClient";
            }
        }

        public override string FieldsTypes
        {
            get
            {
                return "int,int,int,int,int,int,int";
            }
        }

        public override string PrimaryTypes
        {
            get
            {
                return "primary,none,none,none,none,none,none";
            }
        }

        public override string ServerFields
        {
            get
            {
                return "guideId,canSkip,,,trustClient";
            }
        }

        public override string TableFullName
        {
            get
            {
                return "Guide.csv";
            }
        }

        public override string TableHeader
        {
            get
            {
                return "7,1,9999,7";
            }
        }

        public override void BuildRow(object rowData)
        {
            GuideTableRowData guideRow = rowData as GuideTableRowData;
            curBuilder.Append(guideRow.guideId).Append(",");
            curBuilder.Append(guideRow.canSkip).Append(",");
            curBuilder.Append(guideRow.keyStep).Append(",");
            curBuilder.Append(guideRow.hasNextGuide).Append(",");
            curBuilder.Append(guideRow.nextGuideId).Append(",");
            curBuilder.Append(guideRow.nextGuideStepId).Append(",");
            curBuilder.Append(guideRow.trustClient).Append(",");

        }
    }

}
