﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class SerializeTest : EditorWindow
{
    //[MenuItem("GuideEditor/Excel输出测试")]
    static void ExportExcel () {
        Guide.Editor.Serialize.SerializeWriter sWrite = new Guide.Editor.Serialize.SerializeWriter();
        /*
         * 准备，文件名，行，列
         */
        sWrite.PrepareWrite("Test.xlsx", 2, 3);
        /*
         * 表头
         */
        Guide.Editor.Serialize.TableHead head1 = new Guide.Editor.Serialize.TableHead("testTip1","ID", "int", "primary", "id");
        Guide.Editor.Serialize.TableHead head2 = new Guide.Editor.Serialize.TableHead("testTip2", "Name", "string", "index", "name");
        Guide.Editor.Serialize.TableHead head3 = new Guide.Editor.Serialize.TableHead("testTip3", "Content", "string", "none", "content");
        sWrite.AddTableHead(head1);
        sWrite.AddTableHead(head2);
        sWrite.AddTableHead(head3);
        /*
         * 数据, 行，从0开始，并不是Excel中的第0行，而是指第一行数据
         * 数据字符串数组
         */
        sWrite.AddTableData(0, "1000", "小明", "大侠");
        sWrite.AddTableData(1, "1001", "小红", "忍者");

        /*
         * 输出到文件
         */
        sWrite.WriteToExcel(Application.dataPath + "/sample.xlsx");
    }
    //[MenuItem("GuideEditor/Excel读取测试")]
    static void ReadExcel()
    {
        Guide.Editor.Serialize.SerializeReader sReader = new Guide.Editor.Serialize.SerializeReader();

        sReader.ReadFromExcel(Application.dataPath + "/GuideStepExtra.xlsx");

        List<string> secondColumnDataList = new List<string>();
        List<string> secondColumnDataExceptEmptyList = new List<string>();

        sReader.GetColumnAllData(1, secondColumnDataList);
        sReader.GetColumnDataExceptEmpty(1, secondColumnDataExceptEmptyList);

        foreach (var data in secondColumnDataList)
            Debug.Log(data + ",");
        Debug.Log("\n");
        foreach (var data in secondColumnDataExceptEmptyList)
            Debug.Log(data + ",");
    }
}
