﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Guide.Editor.Serialize
{
    public class TableLine
    {
        private string[] lineData = null;

        public TableLine(int length)
        {
            if (length > 0)
                lineData = new string[length];
            else
                Debug.LogError("第二长度必须大于0。");
        }
        public int GetLength()
        {
            if (null == lineData)
                return 0;
            return lineData.Length;
        }
        public void GetAllData(List<string> dataList)
        {
            if(null != dataList)
            {
                dataList.AddRange(lineData);
            }
        }
        public void GetAllExceptEmpty(List<string> dataList)
        {
            if(null != dataList)
            {
                for(int pos = 0; pos < lineData.Length; ++pos)
                {
                    if(!string.IsNullOrEmpty(lineData[pos]))
                    {
                        dataList.Add(lineData[pos]);
                    }
                }
            }
        }

        public void AddData(int pos, string content)
        {
            if(pos >= 0 && pos < lineData.Length)
            {
                lineData[pos] = content;
            }
        }
        public void AddData(params string[] contents)
        {
            for(int pos = 0; pos < contents.Length && pos < lineData.Length; ++pos)
            {
                lineData[pos] = contents[pos];
            }
        }
        public void AddData(int beginPos, params string[] contents)
        {
            for (int pos = beginPos; pos < contents.Length && pos < lineData.Length; ++pos)
            {
                lineData[pos] = contents[pos];
            }
        }
    }
    public class TableContent
    {
        private TableLine[] allData;
        public int GetLength()
        {
            if (null == allData)
                return 0;
            return allData.Length;
        }
        public TableContent(int firstLength, int secondLength)
        {
            if (firstLength > 0)
            {
                allData = new TableLine[firstLength];
                for(int pos = 0; pos < firstLength; ++pos)
                {
                    allData[pos] = new TableLine(secondLength);
                }
            }  
            else
                Debug.LogError("第一长度必须大于0。");
        }
        public void GetLineData(int pos, List<string> dataList)
        {
            if (null != allData && pos < allData.Length)
            {
                allData[pos].GetAllData(dataList);
            }
        }
        public void GetLineDataExceptEmpty(int pos, List<string> dataList)
        {
            if (null != allData && pos < allData.Length)
            {
                allData[pos].GetAllExceptEmpty(dataList);
            }
        }
        public void SetContents(int pos, params string[] contents)
        {
            if(null != allData && pos < allData.Length)
            {
                allData[pos].AddData(contents);
            }
        }
        public void SetContents(int pos, int beginPos, params string[] contents)
        {
            if (null != allData && pos < allData.Length)
            {
                allData[pos].AddData(beginPos,contents);
            }
        }
        public void SetContent(int firstPos, int secondPos, string content)
        {
            if (null != allData && firstPos < allData.Length)
            {
                allData[firstPos].AddData(secondPos, content);
            }
        }
    }
}