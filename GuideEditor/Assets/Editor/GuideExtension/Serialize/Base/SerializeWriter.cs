﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using Excel;
using OfficeOpenXml;
using System.Data;
using UnityEngine;

namespace Guide.Editor.Serialize
{
    public class SerializeWriter
    {
        private TableStructure tableStructure = null;

        private TableContent rowContainer = null;
        private TableContent columnContainer = null;

        /// <summary>
        /// 检查是否已经调用过准备函数
        /// </summary>
        /// <returns></returns>
        private bool CheckPreparation()
        {
            if (null == tableStructure || null == rowContainer || null == columnContainer)
                return false;
            return true;
        }

        /// <summary>
        /// 准备写入
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="rows"></param>
        /// <param name="columns"></param>
        public void PrepareWrite(string tableName, int rows, int columns)
        {
            if (null == tableStructure)
                tableStructure = new TableStructure(tableName);

            if(null == rowContainer)
                rowContainer = new TableContent(rows, columns);

            if (null == columnContainer)
                columnContainer = new TableContent(columns, rows);     
        }
        /// <summary>
        /// 加入表头
        /// </summary>
        /// <param name="tableHead"></param>
        public void AddTableHead(TableHead tableHead)
        {
            if(null != tableStructure)
            {
                tableStructure.AddTableHead(tableHead);
            }
        }

        public void AddTableData(int row, params string[] contents)
        {
            if(null != rowContainer)
            {
                rowContainer.SetContents(row, contents);
            }
            if(null != columnContainer)
            {
                for (int pos = 0; pos < contents.Length; ++pos)
                    columnContainer.SetContent(pos, row, contents[pos]);
            }
        }

        public void AddTableDataByBeginPos(int row, int beginPos, params string[] contents)
        {
            if (null != rowContainer)
            {
                rowContainer.SetContents(row, beginPos, contents);
            }
            if (null != columnContainer)
            {
                for (int pos = beginPos; pos < contents.Length; ++pos)
                    columnContainer.SetContent(pos, row, contents[pos]);
            }
        }

        public bool WriteToExcel(string path)
        {
            if(!CheckPreparation())
            {
                Debug.LogError("请先调用PrepareWrite函数。");
                return false;
            }

            string filePath = path;
            SerializeUtilities.CheckExcelFileName(ref filePath);
            
            if(File.Exists(filePath))
            {
                string tempFile = SerializeUtilities.GetFileReName(Path.GetFileName(path));
                FileInfo orignalFile = new FileInfo(filePath);
                orignalFile.MoveTo(tempFile);
            }
            FileInfo writeFile = new FileInfo(filePath);
            using (ExcelPackage package = new ExcelPackage(writeFile))
            {
                ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("Sheet1");
                int tableHeadCount = tableStructure.GetTableHeadLength();
                worksheet.Cells[1, 1].Value = "7";
                worksheet.Cells[1, 2].Value = "1";
                worksheet.Cells[1, 3].Value = "9999";
                worksheet.Cells[1, 4].Value = tableHeadCount.ToString();
                for(int pos = 0; pos < tableHeadCount; ++pos)
                {
                    TableHead tableHead = tableStructure.GetTableHead(pos);
                    worksheet.Cells[2, pos + 1].Value = tableHead.tips;
                    worksheet.Cells[3, pos + 1].Value = tableHead.keywordClient;
                    worksheet.Cells[4, pos + 1].Value = SerializeUtilities.GetElementTypeStr(tableHead.elementType);
                    worksheet.Cells[5, pos + 1].Value = SerializeUtilities.GetIndexTypeStr(tableHead.indexType);
                    worksheet.Cells[6, pos + 1].Value = tableHead.keywordServer;
                }
               
                int rowCount = rowContainer.GetLength();
                for(int pos = 0; pos < rowCount; ++pos)
                {
                    List<string> dataList = new List<string>();
                    int rowNumber = 7 + pos;
                    rowContainer.GetLineData(pos, dataList);
                    for(int begin = 0; begin < dataList.Count; ++begin)
                    {
                        worksheet.Cells[rowNumber, 1 + begin].Value = dataList[begin];
                    }
                }
               
                package.Save();
            }

            return true;
        }
    }
}
