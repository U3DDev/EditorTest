﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Data;
using UnityEngine;

namespace Guide.Editor.Serialize
{
    public static class SerializeUtilities
    {
        /// <summary>
        /// 该方法只是截取最后一个\\或/后的字符串
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static string GetFileName(string path)
        {
            if (!string.IsNullOrEmpty(path))
            {
                string[] splitName = path.Split('\\', '/');
                if (splitName.Length > 0)
                {
                    return splitName[splitName.Length - 1];
                }
            }
            return string.Empty;
        }
        /// <summary>
        /// 该方法，检查是否有xlsx扩展名
        /// </summary>
        /// <param name="fileName"></param>
        public static void CheckExcelFileName(ref string fileName)
        {
            if (string.IsNullOrEmpty(fileName))
                return;

            if (!fileName.Contains(".xlsx"))
            {
                fileName += ".xlsx";
            }
        }
        /// <summary>
        /// 返回要覆盖源文件前，复制备份一份的路径及文件名
        /// 多线程时不要使用
        /// </summary>
        /// <returns></returns>
        public static string GetFileReName(string tableName)
        {
            string dataPath = Application.dataPath;
            dataPath += "\\Temp\\";
            if(!Directory.Exists(dataPath))
            {
                Directory.CreateDirectory(dataPath);
            }
            dataPath += tableName;
            if (File.Exists(dataPath))
            {
                File.Delete(dataPath);
            }
            return dataPath;
        }

        public static TableElementType VerifyElementType(string elementType)
        {
            if (elementTypeDic.ContainsKey(elementType))
                return elementTypeDic[elementType];
            return TableElementType.TError;
        }

        public static TableIndexType VerifyIndexType(string indexType)
        {
            if (indexTypeDic.ContainsKey(indexType))
                return indexTypeDic[indexType];
            return TableIndexType.TError;
        }

        public static string GetElementTypeStr(TableElementType eType)
        {
            if (elementTypeReverseDic.ContainsKey(eType))
                return elementTypeReverseDic[eType];
            return string.Empty;
        }

        public static string GetIndexTypeStr(TableIndexType iType)
        {
            if (indexTypeReverseDic.ContainsKey(iType))
                return indexTypeReverseDic[iType];
            return string.Empty;
        }

        private static Dictionary<string, TableElementType> elementTypeDic = new Dictionary<string, TableElementType>() {
            { "int", TableElementType.TInt},
            { "double", TableElementType.TDouble},
            { "string", TableElementType.TString},
            { "array_int", TableElementType.TIntArray},
            { "array_double", TableElementType.TDoubleArray},
            { "array_string", TableElementType.TStringArray}
        };
        private static Dictionary<string, TableIndexType> indexTypeDic = new Dictionary<string, TableIndexType>() {
            { "primary", TableIndexType.TPrimary},
            { "index", TableIndexType.TIndex},
            { "none", TableIndexType.TNone}
        };
        private static Dictionary<TableElementType, string> elementTypeReverseDic = new Dictionary<TableElementType, string>() {
            { TableElementType.TInt, "int"},
            { TableElementType.TDouble, "double"},
            { TableElementType.TString, "string"},
            { TableElementType.TIntArray, "array_int"},
            { TableElementType.TDoubleArray, "array_double"},
            { TableElementType.TStringArray, "array_string"}
        };
        private static Dictionary<TableIndexType, string> indexTypeReverseDic = new Dictionary<TableIndexType, string>() {
            { TableIndexType.TPrimary, "primary"},
            { TableIndexType.TIndex, "index"},
            { TableIndexType.TNone, "none"}
        };
    }
}