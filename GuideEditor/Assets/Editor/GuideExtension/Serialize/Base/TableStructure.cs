﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Guide.Editor.Serialize
{
    /// <summary>
    /// 表头类型
    /// </summary>
    public enum TableElementType
    {
        TError = -1,
        TInt = 0,
        TDouble,
        TString,
        TIntArray,
        TDoubleArray,
        TStringArray,
    }
    public enum TableIndexType
    {
        TError = -1,
        TPrimary = 0,
        TIndex,
        TNone,
    }
    /// <summary>
    /// 表头定义
    /// </summary>
    public class TableHead
    {
        public string tips;
        public string keywordClient;
        public TableElementType elementType;
        public TableIndexType indexType;
        public string keywordServer;

        public TableHead(string tips, string keywordClient, string elementType, string indexType, string keywordServer)
        {
            this.tips = tips;
            this.keywordClient = keywordClient;
            this.elementType = SerializeUtilities.VerifyElementType(elementType);
            if(TableElementType.TError == this.elementType)
            {
                Debug.LogError("表不支持此种数据类型。" + elementType);
            }
            this.indexType = SerializeUtilities.VerifyIndexType(indexType);
            if(TableIndexType.TError == this.indexType)
            {
                Debug.LogError("表不支持此种索引类型。" + indexType);
            }
            this.keywordServer = keywordServer;
        }
    }
    public class TableStructure
    {
        /// <summary>
        /// 表名
        /// </summary>
        public string TableName { get; set; }
        /// <summary>
        /// 表头存储列表
        /// </summary>
        private List<TableHead> tableHeadList = new List<TableHead>();
        private Dictionary<string, int> kwClientDic = new Dictionary<string, int>();
        private Dictionary<string, int> kwServerDic = new Dictionary<string, int>();
        private bool hasPrimary = false;

        public TableStructure(string tableName)
        {
            TableName = tableName;
        }

        public int GetTableHeadLength()
        {
            return tableHeadList.Count;
        }
        public TableHead GetTableHead(int pos)
        {
            if(pos >=0 && pos < tableHeadList.Count)
            {
                return tableHeadList[pos];
            }
            return null;
        }
        /// <summary>
        /// 添加表头
        /// </summary>
        /// <param name="tableHead"></param>
        /// <returns>返回列表中的位置，如果出错，返回-1</returns>
        public int AddTableHead(TableHead tableHead)
        {
            if (null == tableHead)
                return -1;

            if(string.IsNullOrEmpty(TableName))
            {
                Debug.LogError("表名还未设置。");
                return -1;
            }

            if(!string.IsNullOrEmpty(tableHead.keywordClient) && kwClientDic.ContainsKey(tableHead.keywordClient))
            {
                Debug.LogError("客户端key已经存在 --- " + TableName+" Key:"+tableHead.keywordClient);
                return -1;
            }

            if (!string.IsNullOrEmpty(tableHead.keywordServer) && kwClientDic.ContainsKey(tableHead.keywordServer))
            {
                Debug.LogError("服务器key已经存在 --- " + TableName + " Key:" + tableHead.keywordServer);
                return -1;
            }

            if(hasPrimary && TableIndexType.TPrimary == tableHead.indexType)
            {
                Debug.LogError("Primary主键已经存在 --- " + TableName);
                return -1;
            }

            if (!hasPrimary)
                hasPrimary = TableIndexType.TPrimary == tableHead.indexType;

            if (!string.IsNullOrEmpty(tableHead.keywordClient))
            {
                kwClientDic.Add(tableHead.keywordClient, tableHeadList.Count);
            }
            if (!string.IsNullOrEmpty(tableHead.keywordServer))
            {
                kwServerDic.Add(tableHead.keywordServer, tableHeadList.Count);
            }
            tableHeadList.Add(tableHead);
            return tableHeadList.Count - 1;
        }
        /// <summary>
        /// 通过位置得到类型
        /// </summary>
        /// <param name="pos"></param>
        /// <returns></returns>
        public TableElementType GetElementType(int pos)
        {
            if(pos < tableHeadList.Count)
            {
                TableHead tableHead = tableHeadList[pos];
                return tableHead.elementType;
            }
            return TableElementType.TError;
        }
        /// <summary>
        /// 通过客户端关键字得到类型
        /// </summary>
        /// <param name="keywordClient"></param>
        /// <returns></returns>
        public TableElementType GetElementType(string keywordClient)
        {
            if(kwClientDic.ContainsKey(keywordClient))
            {
                return GetElementType(kwClientDic[keywordClient]);
            }
            return TableElementType.TError;
        }
    }
}

