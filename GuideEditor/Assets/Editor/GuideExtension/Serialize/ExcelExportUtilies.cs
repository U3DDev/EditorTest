using System.Collections.Generic;
using SimpleNodeBasedEditor;
using UnityEditor;
using UnityEngine;
using System.IO;
using System;
using OfficeOpenXml;

namespace Guide.Editor
{
    public class ExcelExportUtilies
    {
        public static void ExportToExcel(MainDataScriptableObject mainData, string saveFolder)
        {
            List<NodeWindowScriptableObject> windowAssets = new List<NodeWindowScriptableObject>();
            foreach (var pageData in mainData.DataList)
            {
                if (null != pageData)
                {
                    NodeWindowScriptableObject scriptData = AssetDatabase.LoadAssetAtPath<NodeWindowScriptableObject>(mainData.GetPathById(pageData.pageId));
                    if (null != scriptData)
                    {
                        windowAssets.Add(scriptData);
                    }
                    else
                    {
                        Debug.LogError("主文件里索引的路径 实际文件不存在 Path:" + pageData.savePath);
                    }
                }

            }
            //Serialize GuideTable&GuideExtraTable
            GuideTableExcelWriter guideTableWriter = new GuideTableExcelWriter();
            GuideStepTableExcelWriter guideStepTableWriter = new GuideStepTableExcelWriter();
            List<GuideTableRow> guideTableRows = new List<GuideTableRow>();
            List<GuideStepTableRow> stepTableRows = new List<GuideStepTableRow>();
            foreach (var eachAsset in windowAssets)
            {
                foreach (var nodeAsset in eachAsset.NodeDict)
                {
                    switch (nodeAsset.NodeViewType)
                    {
                        case "Guide.Editor.GuideMainStepNodeView":
                            {
                                GuideMainStepExtensionData extenData = nodeAsset.ExtensionData as GuideMainStepExtensionData;
                                GuideTableRow rowData = new GuideTableRow();
                                rowData.canSkip = extenData.canSkip ? "1" : "0";
                                rowData.guideId = extenData.guideId.ToString();
                                rowData.keyStep = extenData.keyStep.ToString();
                                rowData.hasNextGuide = extenData.hasNextStep ? "1" : "0";
                                rowData.nextGuideId = extenData.nextGuideId.ToString();
                                rowData.nextGuideStepId = extenData.nextGuideStepId.ToString();
                                rowData.trustClient = extenData.trustClient ? "1" : "0";
                                rowData.guideTriggerPrefab = extenData.guideTriggerPrefab;
                                guideTableRows.Add(rowData);
                            }
                            break;

                        case "Guide.Editor.GuideNodeViewStep":
                            {
                                GuideStepExtensionData extenData = nodeAsset.ExtensionData as GuideStepExtensionData;
                                if (extenData.stepIndex == -9999)
                                {
                                    continue;
                                }
                                GuideStepTableRow rowData = new GuideStepTableRow();
                                rowData.belongGuideId = extenData.belongGuideId;
                                rowData.stepIndex = extenData.stepIndex;
                                rowData.Rect = extenData.dialogRect;
                                rowData.environmentPrefab = extenData.environmentPrefab;
                                rowData.operationType = extenData.operationType;
                                rowData.operationParam = extenData.operationParam;
                                rowData.maskType = ((int)extenData.maskType).ToString();
                                rowData.dialogType = ((int)extenData.dialogType).ToString();
                                rowData.isTransparent = extenData.isTransparent ? "1" : "0";
                                rowData.isBlockRaycast = extenData.isBlockRaycast ? "1" : "0";
                                rowData.TextModuleId = extenData.TextModuleId;
                                rowData.FrameType = ((int)extenData.FrameType).ToString();
                                rowData.SkipButtonType = ((int)extenData.SkipButtonType).ToString();
                                stepTableRows.Add(rowData);
                            }
                            break;

                    }
                }
            }



            guideTableWriter.SetRowCount(guideTableRows.Count);
            guideStepTableWriter.SetRowCount(stepTableRows.Count);

            guideTableWriter.AddTableHead();
            guideStepTableWriter.AddTableHead();

            for (int i = 0; i < guideTableRows.Count; i++)
            {
                guideTableWriter.AddRow(i, guideTableRows[i]);
            }

            for (int i = 0; i < stepTableRows.Count; i++)
            {
                guideStepTableWriter.AddRow(i, stepTableRows[i]);

            }
            guideTableWriter.SaveToExcel(saveFolder);
            guideStepTableWriter.SaveToExcel(saveFolder);





        }

        public static void ImportToScriptableObjects()
        {

        }

        internal static void ImportToScriptableObjects(EnumDefineScriptableObject enumObj, string folderPath, string excelgenFolderPath)
        {
            Serialize.SerializeReader guideTableReader = new Serialize.SerializeReader();
            Serialize.SerializeReader guideExtraTableReader = new Serialize.SerializeReader();
            Serialize.SerializeReader stepTableReader = new Serialize.SerializeReader();
            Serialize.SerializeReader stepExtraTableReader = new Serialize.SerializeReader();
            DirectoryInfo guideFolder = new DirectoryInfo(folderPath);
            FileInfo[] files = guideFolder.GetFiles();
            foreach (var eachFile in files)
            {
                Debug.Log("FileName :" + eachFile.Name);
                if (eachFile.Name == "GuideTrigger.xlsx")
                {
                    Serialize.SerializeReader sReader = new Serialize.SerializeReader();
                    sReader.ReadFromExcel(eachFile.FullName);
                    List<string> triggerList = new List<string>();
                    sReader.GetColumnDataExceptEmpty(0, triggerList);
                    enumObj.Triggers = triggerList;
                }
                if (eachFile.Name == "GuideStepButtonPath.xlsx")
                {
                    Serialize.SerializeReader sReader = new Serialize.SerializeReader();
                    sReader.ReadFromExcel(eachFile.FullName);
                    List<string> buttons = new List<string>();
                    sReader.GetColumnDataExceptEmpty(0, buttons);
                    enumObj.Buttons = buttons;

                }
                if (eachFile.Name == "GuideStepEnvironment.xlsx")
                {
                    Serialize.SerializeReader sReader = new Serialize.SerializeReader();
                    sReader.ReadFromExcel(eachFile.FullName);
                    List<string> stepEnvs = new List<string>();
                    sReader.GetColumnDataExceptEmpty(0, stepEnvs);
                    enumObj.EnvPrefabs = stepEnvs;
                }
                if (eachFile.Name == "Guide.xlsx")
                {
                    guideTableReader.ReadFromExcel(eachFile.FullName);
                }
                if (eachFile.Name == "GuideExtra.xlsx")
                {
                    guideExtraTableReader.ReadFromExcel(eachFile.FullName);
                }
                if (eachFile.Name == "GuideStep.xlsx")
                {
                    stepTableReader.ReadFromExcel(eachFile.FullName);
                }
                if (eachFile.Name == "GuideStepExtra.xlsx")
                {
                    stepExtraTableReader.ReadFromExcel(eachFile.FullName);
                }
            }



            //生成Node数据
            //int nodeCount = stepTableReader.GetRowLength();
            //List<NodeDataScriptableObject> nodesScriptableObjects = new List<NodeDataScriptableObject>();
            //for (int i = 0; i < nodeCount; i++)
            //{
            //    NodeDataScriptableObject nodeObject = ScriptableObject.CreateInstance<NodeDataScriptableObject>();
            //    nodeObject.Id = i;
            //    nodeObject.NodeName = "引导节点" + i;
            //    //主节点 250*250 
            //    //Step节点 250*400
            //    nodeObject.windowRect = new Rect(250 * (i % 7), 400 * (i / 3), 250, 400);
            //    nodeObject.NodeViewType = typeof(GuideNodeViewStep).FullName;
            //    nodeObject.LinkId = i + 1;
            //    nodesScriptableObjects.Add(nodeObject);
            //}
            

            //生成Window数据

            //生成主索引文件


            //保存数据
            EditorUtility.SetDirty(enumObj);
            AssetDatabase.SaveAssets();
        }
    }



}