﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;
using SimpleNodeBasedEditor;
namespace Guide.Editor
{
    public class GuideMainEditorWindow : EditorWindow
    {
        /// <summary>
        /// 源数据存放位置
        /// </summary>
        private string excelFolderPath;
        /// <summary>
        ///生成的编辑器数据存放位置
        /// </summary>
        private string genAssetFolderPath;
        /// <summary>
        /// excel生产位置
        /// </summary>
        private string excelgenFolderPath;
        private string enumDefinePath;
        private MainDataScriptableObject mainData;
        public static EnumDefineScriptableObject EnumDefineData;

        public static int value = 1;
        [MenuItem("引导编辑器/编辑器主区域")]
        static void ShowEditor()
        {
            //必须打开数据助手窗口
            ShowDataTool();
            MainNodeEditorWindow editor = EditorWindow.GetWindow<MainNodeEditorWindow>("节点编辑器", true);

        }

        [MenuItem("引导编辑器/数据助手")]
        static void ShowDataTool()
        {
            GuideMainEditorWindow editor = EditorWindow.GetWindow<GuideMainEditorWindow>("数据助手", true);

        }

        private void OnEnable()
        {

            excelFolderPath = EditorPrefs.GetString("excelFolderPath", "");
            genAssetFolderPath = EditorPrefs.GetString("genAssetFolderPath", "");
            excelgenFolderPath = EditorPrefs.GetString("excelgenFolderPath", "");
            enumDefinePath = EditorPrefs.GetString("enumDefinePath", "");
        }
        private void OnGUI()
        {
            GUILayout.BeginHorizontal();
           
            if (GUILayout.Button("点击选择引导数据表文件夹"))
            {
                excelFolderPath = EditorUtility.OpenFolderPanel("请选择引导数据表文件夹", Application.dataPath, "");
                EditorPrefs.SetString("excelFolderPath", excelFolderPath);
            }
            if (!string.IsNullOrEmpty(excelFolderPath))
            {
                GUILayout.Label("导入数据路径 :" + excelFolderPath);
            }
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            
            if (GUILayout.Button("点击选择生成数据存放路径"))
            {
                genAssetFolderPath = EditorUtility.OpenFolderPanel("请选择生成数据存放路径", Application.dataPath, "");
                EditorPrefs.SetString("genAssetFolderPath", genAssetFolderPath);
            }
            if (!string.IsNullOrEmpty(genAssetFolderPath))
            {
                GUILayout.Label("Scriptableobject保存路径 :" + genAssetFolderPath);
            }
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            if (GUILayout.Button("点击选择生成excel存放路径"))
            {
                var selectedPath = EditorUtility.OpenFolderPanel("请选择引导数据表文件夹", Application.dataPath, "");
                if (null != selectedPath)
                {
                    excelgenFolderPath = selectedPath;
                    EditorPrefs.SetString("excelgenFolderPath", excelgenFolderPath);
                }
            }
            if (!string.IsNullOrEmpty(excelgenFolderPath))
            {
                GUILayout.Label("导出Excel路径 :" + excelgenFolderPath);
            }

            GUILayout.EndHorizontal();
            if (GUILayout.Button(new GUIContent("生成Scriptableobject数据")))
            {
                if (EnumDefineData == null)
                {
                    EditorUtility.DisplayDialog("生成失败", "请选择EnumDefine文件", "确定");
                    return;
                }
                if (string.IsNullOrEmpty(genAssetFolderPath))
                {
                    var selectedPath = EditorUtility.OpenFolderPanel("请选择生成数据存放路径", Application.dataPath, "");
                    if (null!=selectedPath)
                    {
                        genAssetFolderPath = selectedPath;
                    }
                    EditorPrefs.SetString("genAssetFolderPath", genAssetFolderPath);
                }
                if (string.IsNullOrEmpty(excelFolderPath))
                {
                    var selectedPath = EditorUtility.OpenFolderPanel("请选择引导数据表文件夹", Application.dataPath, "");
                    if (null!=selectedPath)
                    {
                        excelFolderPath = selectedPath;
                        EditorPrefs.SetString("excelFolderPath", excelFolderPath);
                    }

                }
                ImportToScriptableObject(excelFolderPath, genAssetFolderPath);

            }
          
            mainData = (MainDataScriptableObject)EditorGUILayout.ObjectField(mainData, typeof(MainDataScriptableObject), false);
            if (mainData == null)
            {
                var loadedObj = AssetDatabase.LoadAssetAtPath<MainDataScriptableObject>(EditorDataSaveUtilities.Instance.MainFileSavePath);
                if (loadedObj != null)
                {
                    mainData = loadedObj;
                }
            }
            EnumDefineData = (EnumDefineScriptableObject)EditorGUILayout.ObjectField(EnumDefineData, typeof(EnumDefineScriptableObject), false);
            if (EnumDefineData == null)
            {
                var loadedObj = AssetDatabase.LoadAssetAtPath<EnumDefineScriptableObject>(enumDefinePath);
                if (loadedObj != null)
                {
                    EnumDefineData = loadedObj;
                }
            }
            if (GUILayout.Button(new GUIContent("导出Excel")))
            {
                if (mainData == null)
                {
                    EditorUtility.DisplayDialog("导出异常", "请选择主数据文件", "确定");
                }

                else if (string.IsNullOrEmpty(excelgenFolderPath))
                {

                    var path = EditorUtility.OpenFolderPanel("请选择Excel导出路径", Application.dataPath, "");
                    while (path == null)
                    {
                        path = EditorUtility.OpenFolderPanel("请选择Excel导出路径", Application.dataPath, "");
                    }
                    excelgenFolderPath = path;


                    EditorPrefs.SetString("excelgenFolderPath", excelgenFolderPath);
                }
                else
                {
                    ExportToExcel(mainData, excelgenFolderPath);

                }
            }

            if (EnumDefineData != null)
            {
                EditorPrefs.SetString("enumDefinePath", AssetDatabase.GetAssetPath(EnumDefineData));
            }

        }
        private void ExportToExcel(MainDataScriptableObject mainData, string excelgenFolderPath)
        {
            ExcelExportUtilies.ExportToExcel(mainData, excelgenFolderPath);
        }
        private void ImportToScriptableObject(string folderPath,string genPath)
        {
            
            ExcelExportUtilies.ImportToScriptableObjects(EnumDefineData,folderPath, excelgenFolderPath);

        }
        void Update()
        {
            if (EditorApplication.isCompiling)
            {
                mainData = null;
                EnumDefineData = null;
                Close();
            }

        }

    }
}