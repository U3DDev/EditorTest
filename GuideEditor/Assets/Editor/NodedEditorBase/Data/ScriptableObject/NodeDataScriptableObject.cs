﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
namespace SimpleNodeBasedEditor
{
    public class NodeDataScriptableObject : ScriptableObject
    {
        public int Id;
        public string NodeName;
        public Rect windowRect;
        public int LinkId;
        
        public ScriptableObject ExtensionData;
        public string NodeViewType;
    }
 
}