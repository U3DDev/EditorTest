﻿using UnityEditor;
using UnityEngine;
using System.Collections.Generic;
namespace SimpleNodeBasedEditor
{
    public class NodeWindowScriptableObject : ScriptableObject
    {
        public string WindowName;
        //引用view层对象 有待更改
        public List<NodeDataScriptableObject> NodeDict;
        public int Counter;
        public int PageId;
        public string SaveFolder;
        public string WindowType;
        public ScriptableObject ExtensionData;

        public void FillData(BaseNodeWindowData windowData)
        {
            WindowName = windowData.WindowName;
            NodeDict = new List<NodeDataScriptableObject>();
            foreach (var data in windowData.NodeDict)
            {
                if (data != null)
                {
                    NodeDataScriptableObject scriptableObj = ScriptableObject.CreateInstance<NodeDataScriptableObject>();
                    scriptableObj.Id = data.Id;
                    scriptableObj.LinkId = data.LinkId;
                    scriptableObj.NodeName = data.NodeName;
                    scriptableObj.windowRect = data.windowRect;
                    scriptableObj.NodeViewType = data.NodeViewType;
                    scriptableObj.ExtensionData = data.ExtensionData;
                    NodeDict.Add(scriptableObj);
                }
            }
            SaveFolder = windowData.SaveFolder;
            Counter = windowData.Counter;
            PageId = windowData.PageId;
            WindowType = windowData.WindowType;
            ExtensionData = windowData.ExtensionObject;
        }
        public List<BaseNodeData> ToNodeDataList()
        {
            List<BaseNodeData> dataList = new List<BaseNodeData>();
            if (NodeDict != null)
            {
                foreach (var scriptObj in NodeDict)
                {
                    if (scriptObj != null)
                    {
                        BaseNodeData data = new BaseNodeData();
                        data.Id = scriptObj.Id;
                        data.LinkId = scriptObj.LinkId;
                        data.NodeName = scriptObj.NodeName;
                        data.windowRect = scriptObj.windowRect;
                        data.NodeViewType = scriptObj.NodeViewType;
                        data.ExtensionData = scriptObj.ExtensionData;
                        dataList.Add(data);
                    }

                }

            }
            return dataList;
        }

    }

}