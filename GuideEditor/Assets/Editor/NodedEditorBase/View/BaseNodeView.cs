﻿using UnityEngine;
using System.Collections;
using System;
using UnityEditor;
namespace SimpleNodeBasedEditor
{
    public class BaseNodeRenamePopWindow : EditorWindow
    {

        private string nameText = "";
        private Action<string> onClick;
        public void Init(Action<string> m_callback, string title)
        {
            onClick = m_callback;
            titleContent.text = title;
        }
        private void OnGUI()
        {

            GUILayout.Label("新名称");
            nameText = GUILayout.TextField(nameText);
            if (GUILayout.Button("确定") && !string.IsNullOrEmpty(nameText))
            {
                Close();
                if (null != onClick)
                {
                    onClick(nameText);
                }

            }
            this.Focus();
        }
    }

    [BaseNode(false, "基本节点")]
    /// <summary>
    /// 每个节点的基类
    /// </summary>
    public class BaseNodeView
    {
        private BaseNodeData m_data;

        public BaseNodeData Data
        {
            get
            {
                return m_data;
            }

            set
            {
                m_data = value;
            }
        }
        public BaseNodeView()
        {

        }

        /// <summary>
        /// 新建数据会调用此方法
        /// </summary>
        /// <param name="id"></param>
        /// <param name="type"></param>
        public virtual void CreateData(int id, string type)
        {
            m_data = new BaseNodeData();
            m_data.Id = id;
            m_data.NodeName = "节点" + id;
            m_data.windowRect = new Rect(50 + 150 * (id - 1), 50, 150, 100);
            m_data.NodeViewType = type;
        }

        /// <summary>
        /// 填充已有数据
        /// </summary>
        /// <param name="baseData"></param>
        public virtual void LoadData(BaseNodeData baseData)
        {
            m_data = baseData;

        }

        public virtual void Release()
        {
        }


        public virtual void DrawNode(int id)
        {
            if (GUILayout.Button("更改名称"))
            {
                BaseNodeRenamePopWindow window = ScriptableObject.CreateInstance<BaseNodeRenamePopWindow>();
                window.Init((name) =>
                {
                    m_data.NodeName = name;
                }, m_data.NodeName);
                window.position = new Rect(GUIUtility.GUIToScreenPoint(Event.current.mousePosition), new Vector2(100, 80));
                window.ShowUtility();
            }
            if (GUILayout.Button("删除节点"))
            {
                DeleteNode();
            }

            NodeEditorEventManager.Instance.ProcessNodeEvent(Event.current, id);
            //设置改窗口可以拖动
            GUI.DragWindow();
        }
        #region UI Event
        protected virtual void DeleteNode()
        {
            BaseNodeWindow window = NodeWindowManager.Instance.GetCurrentWindow();
            window.DeleteNode(this.Data.Id);
        }
        public virtual void OnClick(int buttonType)
        {
            BaseNodeWindow window = NodeWindowManager.Instance.GetCurrentWindow();

            //右键
            if (buttonType == 1)
            {
                bool value = window.ReleaseSelect;
                window.ReleaseSelect = !value;
            }
            if (buttonType == 0)
            {
                if (!window.ReleaseSelect)
                {
                    return;
                }
                int lastSelectId = window.CurSelectId;
                if (lastSelectId == -1)
                {
                    return;
                }
                var lastNode = window.GetNode(lastSelectId);
                if (null != lastNode)
                {
                    if (lastNode.Data.Id != Data.Id)
                    {
                        lastNode.Data.LinkId = Data.Id;
                        window.ReleaseSelect = false;
                    }
                }
            }
        }

        public virtual void OnDrag()
        {

        }

        public virtual void SetCenterPosition(Vector2 mousePosition)
        {
            if (null != Data)
            {
                float width = m_data.windowRect.width;
                float height = m_data.windowRect.height;
                m_data.windowRect.position = new Vector2(mousePosition.x, mousePosition.y);
            }
        }

        public static BaseNodeView CreateByType(string nodeViewType)
        {

            BaseNodeView nodeObj = (BaseNodeView)ReflectionUtilities.CreateInstance(nodeViewType);
            if (nodeObj == null)
            {
                Debug.LogError("创建实例失败 " + nodeViewType);
            }
            return nodeObj;
        }

        public virtual void WillSaveData(BaseNodeWindow baseNodeWindow)
        {

        }





        #endregion
    }
}