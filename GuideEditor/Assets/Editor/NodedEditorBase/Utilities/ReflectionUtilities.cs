﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace SimpleNodeBasedEditor
{
    public  class ReflectionUtilities
    {
        public static Type GetType(string fullName,out Assembly assembly)
        {
            IEnumerable<Assembly> scriptAssemblies = AppDomain.CurrentDomain.GetAssemblies();
            foreach (var a in scriptAssemblies)
            {
                foreach (Type t in a.GetTypes())
                {
                    if (t.FullName==fullName)
                    {
                        //UnityEngine.Debug.Log("找到类型 " + fullName);
                        assembly = a;
                        return t;
                    }
                }
            }
            UnityEngine.Debug.LogError("未发现类型 "+fullName);
            assembly = null;
            return null;
        }

        public static object CreateInstance(string fullName)
        {
            IEnumerable<Assembly> scriptAssemblies = AppDomain.CurrentDomain.GetAssemblies();
            foreach (var a in scriptAssemblies)
            {
                //UnityEngine.Debug.Log("Search Asssembly: " + a.FullName);
                foreach (Type t in a.GetTypes())
                {
                    if (t.FullName == fullName)
                    {
                        var obj = Activator.CreateInstance(t);
                        return obj;
                    }
                }
            }
            UnityEngine.Debug.LogError("未找到对应的类型 ！");
            return null;
        }
    }
}
