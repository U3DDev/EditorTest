﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
namespace SimpleNodeBasedEditor
{
    /// <summary>
    /// 管理不同编辑器窗口
    /// </summary>
    public class NodeWindowManager
    {
        private static NodeWindowManager m_instance;
        public static NodeWindowManager Instance
        {

            get
            {

                if (m_instance == null)
                {
                    m_instance = new NodeWindowManager();
                }
                return m_instance;
            }
        }

        private BaseNodeSelectorWindow m_selectorWindow;

        private BaseNodeWindow currentEditWindow;
        private MainDataScriptableObject m_mainData;
        private bool m_isInit = false;

        public MainDataScriptableObject MainData
        {
            get
            {
                return m_mainData;
            }
        }



        public bool IsInit
        {
            get
            {
                return m_isInit;
            }

            set
            {
                m_isInit = value;
            }
        }
        private List<NodeWindowScriptableObject> m_windowAssets = new List<NodeWindowScriptableObject>();

        public List<NodeWindowScriptableObject> WindowAssets
        {
            get
            {
                return m_windowAssets;
            }

            set
            {
                m_windowAssets = value;
            }
        }

        /// <summary>
        /// 打开上次打开的分页
        /// </summary>
        public void LoadLastEditWindow()
        {

            if (m_mainData.hasLastEditorWindow && m_mainData.lastEdiorPageId != -1)
            {
                currentEditWindow = BaseNodeWindow.CreateNodeWindow(m_mainData.lastEdiorPageId);
                currentEditWindow.LoadData(m_mainData.lastEdiorPageId);
            }
 
        }
        public void LoadWindow(int pageId)
        {
            if (m_mainData != null)
            {
                currentEditWindow =BaseNodeWindow.CreateNodeWindow(pageId);
                currentEditWindow.LoadData(pageId);
                //SaveCurrentPage();
            }
            else
            {
                Debug.LogError("主索引文件丢失");
            }
        }
        public void FirstLoad(MainDataScriptableObject mainData)
        {

            SetMainData(mainData);
            PreLoadAssets();
            LoadLastEditWindow();

            m_selectorWindow = new BaseNodeSelectorWindow();
            m_isInit = true;
        }
        public void PreLoadAssets()
        {
            WindowAssets.Clear();
            foreach (var pageData in m_mainData.DataList)
            {
                if (null != pageData)
                {
                    NodeWindowScriptableObject scriptData = AssetDatabase.LoadAssetAtPath<NodeWindowScriptableObject>(NodeWindowManager.Instance.MainData.GetPathById(pageData.pageId));
                    if (null != scriptData)
                    {
                        WindowAssets.Add(scriptData);
                    }
                    else
                    {
                        Debug.LogError("主文件里索引的路径 实际文件不存在 Path:" + pageData.savePath);
                    }
                }

            }
        }

        public void SaveCurrentPage()
        {
            GetCurrentWindow().SaveToData();
            EditorDataSaveUtilities.Instance.SavePage(GetCurrentWindow());
        }

        public BaseNodeWindow CreateNewWindow(string windowType=null)
        {
            if (windowType==null)
            {
                windowType = typeof(BaseNodeWindow).FullName;
                Debug.Log("Default Window Type :"+ windowType);
            }
            currentEditWindow = BaseNodeWindow.CreateNodeWindow(windowType);
            int pageId = m_mainData.DataList != null ? m_mainData.DataList.Count + 1 : 1;
            currentEditWindow.CreateData(pageId,windowType.ToString());
            SaveCurrentPage();
            return currentEditWindow;
        }
        public BaseNodeWindow GetCurrentWindow()
        {
            if (null == currentEditWindow)
            {
                return null;
            }
            return currentEditWindow;
        }
        public BaseNodeSelectorWindow GetSelectorWindow()
        {
            if (null == m_selectorWindow)
            {
                Debug.LogError("Please Init First !When getting selectorWindow");
                return null;
            }
            return m_selectorWindow;
        }
        public void SetMainData(MainDataScriptableObject mainData)
        {
            m_mainData = mainData;
        }
    }
}