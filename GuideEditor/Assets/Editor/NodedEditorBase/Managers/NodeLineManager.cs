﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
namespace SimpleNodeBasedEditor
{

    /// <summary>
    /// 节点之间的连线管理器
    /// </summary>
    public class NodeLineManager
    {
        private static NodeLineManager m_instance;
        public static NodeLineManager Instance
        {
            get
            {
                if (m_instance == null)
                {
                    m_instance = new NodeLineManager();
                }
                return m_instance;
            }
        }


        public void DrawNodeCurve(Rect start, Rect end, Color color)
        {
            Vector3 startPos = new Vector3(start.x + start.width, start.y + start.height / 2, 0);
            Vector3 endPos = new Vector3(end.x, end.y + end.height / 2, 0);
            Vector3 startTan = startPos + Vector3.right * 50;
            Vector3 endTan = endPos + Vector3.left * 50;
            Handles.DrawBezier(startPos, endPos, startTan, endTan, color, null, 4);
        }

    }
}