﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExampleClass : MonoBehaviour
{
    public GUIContent buttonText = new GUIContent("some button");
    public GUIStyle buttonStyle = GUIStyle.none;
    void OnGUI()
    {
        Rect rt = GUILayoutUtility.GetRect(buttonText, buttonStyle);
        if (rt.Contains(Event.current.mousePosition))
            GUI.Label(new Rect(0, 20, 200, 70), "PosX: " + rt.x + "\nPosY: " + rt.y + "\nWidth: " + rt.width + "\nHeight: " + rt.height);

        GUI.Button(rt, buttonText, buttonStyle);
    }
}
