﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace SimpleNodeBasedEditor
{
    /// <summary>
    /// 负责分发鼠标事件
    /// </summary>
    public class NodeEditorEventManager
    {

        private static NodeEditorEventManager m_instance;
        public static NodeEditorEventManager Instance
        {

            get
            {

                if (m_instance == null)
                {
                    m_instance = new NodeEditorEventManager();
                }
                return m_instance;
            }
        }


        public void ProcessWindowEvent(Event e, int pageId)
        {
            var curWindow = NodeWindowManager.Instance.GetCurrentWindow();

            switch (e.type)
            {
                case EventType.MouseDown:

                    if (Event.current.button == 1)
                    {

                        if (curWindow.CurSelectId != -1 && curWindow.ReleaseSelect)
                        {
                            var baseNode = curWindow.GetNode(curWindow.CurSelectId);
                            baseNode.Data.LinkId = -1;
                            curWindow.ReleaseSelect = false;
                            curWindow.CurSelectId = -1;
                            return;
                        }
                    }


                    if (!curWindow.ReleaseSelect)
                    {
                        curWindow.OnClick(Event.current.button);
                    }


                    break;
                case EventType.MouseUp:
                    break;
                case EventType.MouseMove:
                    break;

            }
        }
        public void ProcessNodeEvent(Event e, int id)
        {
            var baseNode = NodeWindowManager.Instance.GetCurrentWindow().GetNode(id);

            switch (e.type)
            {
                case EventType.MouseDown:
                    if (baseNode != null)
                    {
                        try
                        {
                            baseNode.OnClick(e.button);
                        }
                        catch (System.Exception er)
                        {
                            Debug.Log(er);
                        }
                        NodeWindowManager.Instance.GetCurrentWindow().SetSelectId(id);

                    }
                    break;

                case EventType.MouseDrag:
                    if (baseNode != null)
                    {
                        baseNode.OnDrag();
                    }

                    break;
            }
        }




    }
}