﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using UnityEngine;

namespace SimpleNodeBasedEditor
{
    public class BaseNodeWindowTypes
    {
        public static Dictionary<Type, string> GetAllWindowsDict()
        {
            Dictionary<Type, string> windoowTypeDict = new Dictionary<Type, string>();

            IEnumerable<Assembly> scriptAssemblies = AppDomain.CurrentDomain.GetAssemblies();
            foreach (Assembly assembly in scriptAssemblies)
            {
                foreach (Type type in assembly.GetTypes().Where(T => T.IsClass && !T.IsAbstract && T.IsSubclassOf(typeof(BaseNodeWindow))))
                {
                    object[] nodeAttributes = type.GetCustomAttributes(typeof(BaseNodeWindowAttribute), false);
                    BaseNodeWindowAttribute attr = nodeAttributes[0] as BaseNodeWindowAttribute;
                    if (attr == null || !attr.isHide)
                    {
                        Debug.Log("初始化Type： " + type + " Context:" + attr.contextText);
                        windoowTypeDict.Add(type, attr.contextText);
                    }
                }
            }

            return windoowTypeDict;
        }
    }

    public class BaseNodeWindowAttribute : Attribute
    {
        public bool isHide { get; private set; }
        public string contextText { get; private set; }

        /// <summary>
        /// 节点属性
        /// </summary>
        /// <param name="hideWindow">是否隐藏</param>
        /// <param name="displayContext">显示在菜单上的文字</param>
        public BaseNodeWindowAttribute(bool hideWindow, string displayContext)
        {
            isHide = hideWindow;
            contextText = displayContext;
        }


    }
}
