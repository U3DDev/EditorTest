﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace SimpleNodeBasedEditor
{
    public class BaseNodeTypes
    {
        public static Dictionary<Type, string> GetAllNodesDict()
        {
            Dictionary<Type, string> nodeTypeDict = new Dictionary<Type, string>();

            IEnumerable<Assembly> scriptAssemblies = AppDomain.CurrentDomain.GetAssemblies();
            foreach (Assembly assembly in scriptAssemblies)
            {
                foreach (Type type in assembly.GetTypes().Where(T => T.IsClass && !T.IsAbstract && T.IsSubclassOf(typeof(BaseNodeView))))
                {
                    object[] nodeAttributes = type.GetCustomAttributes(typeof(BaseNodeAttribute), false);
                    BaseNodeAttribute attr = nodeAttributes[0] as BaseNodeAttribute;
                    if (attr == null || !attr.isHide)
                    {
                        Debug.Log("初始化Type： " + type + " Context:" + attr.contextText);
                        nodeTypeDict.Add(type, attr.contextText);
                    }
                }
            }

            return nodeTypeDict;
        }
    }

    public class BaseNodeAttribute : Attribute
    {
        public bool isHide { get; private set; }
        public string contextText { get; private set; }

        /// <summary>
        /// 节点属性
        /// </summary>
        /// <param name="HideNode">是否隐藏</param>
        /// <param name="displayContext">显示在菜单上的文字</param>
        public BaseNodeAttribute(bool HideNode, string displayContext)
        {
            isHide = HideNode;
            contextText = displayContext;
        }

    }
}


