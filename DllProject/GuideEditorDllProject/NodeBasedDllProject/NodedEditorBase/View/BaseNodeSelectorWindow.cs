﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;

namespace SimpleNodeBasedEditor
{
    public class BaseNodeSelectorWindow
    {

        public Vector2 SelectorScrollPosition = Vector2.zero;

        public void Draw()
        {
            GUILayout.BeginVertical();
            SelectorScrollPosition = GUILayout.BeginScrollView(SelectorScrollPosition);
            int willGoPageid = -1;
            foreach (var windowData in NodeWindowManager.Instance.WindowAssets)
            {
                if (null != windowData)
                {
                    var content = new GUIContent(windowData.WindowName, "保存路径:" + windowData.SaveFolder);
                    if (GUILayout.Button(content))
                    {

                        willGoPageid = windowData.PageId;

                        Debug.Log("Click On:" + windowData.WindowName);
                    }
                }

            }
            if (willGoPageid != -1)
            {
                bool willChange = EditorUtility.DisplayDialog("未保存的窗口数据将会丢失 请确认保存后再切换窗口!", "是否将要切换?", "确定", "取消");
                if (willChange)
                {
                    NodeWindowManager.Instance.LoadWindow(willGoPageid);
                }
            }
            if (GUILayout.Button("新建窗口"))
            {
                GenericMenu menu = new GenericMenu();
                Dictionary<Type, string> dict = BaseNodeWindowTypes.GetAllWindowsDict();
                if (dict.Count==0)
                {
                    Debug.LogError("没有任何扩展类型");
                }
                foreach (var pair in dict)
                {
                    menu.AddItem(new GUIContent(pair.Value), false, ProcessCreate, pair.Key);
                }
                menu.ShowAsContext();
            }
            GUILayout.EndScrollView();
            GUILayout.EndVertical();
        }
        private void ProcessCreate(object userData)
        {
            Type t = (Type)userData;

            if (t!=null)
            {
                NodeWindowManager.Instance.CreateNewWindow(t.FullName);
            }

        }









    }



}