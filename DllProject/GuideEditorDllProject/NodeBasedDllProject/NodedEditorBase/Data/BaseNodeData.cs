﻿using UnityEngine;
using System.Collections;
namespace SimpleNodeBasedEditor
{
    /// <summary>
    /// 每个节点的数据类
    /// </summary>
    public class BaseNodeData
    {
        public int Id;
        public string NodeName;

        public Rect windowRect;
        public int LinkId;

        public string NodeViewType;

        public ScriptableObject ExtensionData;

        public BaseNodeData()
        {
            //为空但不报错 的标示
            LinkId = -1;
        }

        public BaseNodeData(BaseNodeData data)
        {
            this.Id = data.Id;
            this.NodeName = data.NodeName;
            this.windowRect = data.windowRect;
            this.LinkId = data.LinkId;
            NodeViewType = data.NodeViewType;
            ExtensionData = data.ExtensionData;
        }
    }
}