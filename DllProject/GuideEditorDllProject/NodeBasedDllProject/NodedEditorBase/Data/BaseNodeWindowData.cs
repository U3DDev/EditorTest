﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
namespace SimpleNodeBasedEditor
{
    /// <summary>
    /// 窗口数据类
    /// </summary>
    public class BaseNodeWindowData
    {
        public string WindowName;
        public List<BaseNodeData> NodeDict = new List<BaseNodeData>();
        public int Counter;
        public int PageId;
        public string SaveFolder = "";
        public string WindowType;
        public ScriptableObject ExtensionObject;
    }
}