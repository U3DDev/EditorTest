﻿using System;
using System.Collections.Generic;
using UnityEngine;
using System.Text;
namespace SimpleNodeBasedEditor
{
    /// <summary>
    /// 总管理器
    /// </summary>
    [CreateAssetMenu(fileName = "MainData", menuName = "创建窗口主文件")]
    [SerializeField]
    public class MainDataScriptableObject : ScriptableObject
    {
        [SerializeField]
        public List<PageData> DataList = new List<PageData>();
        public bool hasLastEditorWindow = false;
        public int lastEdiorPageId = -1;
        public string GetPathById(int id)
        {
            if (null != DataList)
            {
                foreach (var pageData in DataList)
                {
                    if (pageData.pageId == id)
                    {
                        return pageData.savePath;
                    }
                }
            }

            return "notFount";
        }
        public PageData GetDataById(int id)
        {
            if (null != DataList)
            {
                foreach (var pageData in DataList)
                {
                    if (pageData.pageId == id)
                    {
                        return pageData;
                    }
                }
            }

            return null;
        }
        public void AddPageData(PageData pageData)
        {
            if (null != DataList && null != pageData)
            {
                foreach (var eachData in DataList)
                {
                    if (eachData.pageId == pageData.pageId)
                    {
                        eachData.pageId = pageData.pageId;
                        eachData.savePath = pageData.savePath;
                        Debug.Log("已经存在 更新数据 ");
                        return;
                    }
                }
                DataList.Add(pageData);

            }
        }


    }

    [System.Serializable]

    public class PageData
    {
        [SerializeField]

        public int pageId;
        [SerializeField]

        public string savePath;
    }
}