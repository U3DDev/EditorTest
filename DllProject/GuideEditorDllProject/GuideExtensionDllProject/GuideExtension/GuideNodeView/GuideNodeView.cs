﻿using UnityEngine;
using System.Collections;
using System;
using UnityEditor;
using SimpleNodeBasedEditor;

namespace Guide.Editor
{

    [BaseNode(false, "引导基本节点")]
    public class GuideNodeViewStep : BaseNodeView
    {

        #region UIElements Field
        private int m_ButtonNameSelectedIndex;
        private int m_EnvSelectedIndex;
        private int m_OperationTypeSelectedIndex;
        private GuideStepMaskType m_maskType;
        private bool m_isTransparent;
        private bool m_isBlockRaycast;

        private string m_operationParamStr;
        private Rect m_dialogRect;

        private GuideStepDialogType m_dialogType = 0;
        private string m_TextModuleId;
        private GuideStepFrameType m_FrameType = 0;
        private GuideStepSkipButtonType m_SkipButtonType = 0;
        #endregion

        /// <summary>
        /// 新建数据会调用此方法
        /// </summary>
        /// <param name="id"></param>
        /// <param name="type"></param>
        public override void CreateData(int id, string type)
        {
            base.CreateData(id, type);
            //步骤类型的换一种大小
            Data.windowRect = new Rect(50 + 150 * (id - 1), 50, 250, 400);
            Data.ExtensionData = ScriptableObject.CreateInstance<GuideStepExtensionData>();
            Debug.Log("GuideStep Create ExtensionData");
        }
        /// <summary>
        /// 已有数据会调用此方法
        /// </summary>
        /// <param name="baseData"></param>
        public override void LoadData(BaseNodeData baseData)
        {
            base.LoadData(baseData);
            if (Data.ExtensionData != null)
            {
                if (Data.ExtensionData.GetType() == typeof(GuideStepExtensionData))
                {
                    GuideStepExtensionData extenData = Data.ExtensionData as GuideStepExtensionData;
                    if (extenData.operationType== "Click")
                    {
                        m_ButtonNameSelectedIndex = GuideStepExtensionData.GetButtonNames().IndexOf(extenData.operationParam);
                    }
                    if (m_ButtonNameSelectedIndex == -1)
                    {
                        m_ButtonNameSelectedIndex = 0;
                    }
                    m_EnvSelectedIndex = GuideStepExtensionData.GetEnvPrefabs().IndexOf(extenData.environmentPrefab);
                    if (m_EnvSelectedIndex == -1)
                    {
                        m_EnvSelectedIndex = 0;
                    }
                    m_OperationTypeSelectedIndex = GuideStepExtensionData.GetOperationTypes().IndexOf(extenData.operationType);
                    if (m_OperationTypeSelectedIndex == -1)
                    {
                        m_OperationTypeSelectedIndex = 0;
                    }
                    m_maskType = extenData.maskType;
                    m_isTransparent = extenData.isTransparent;
                    m_isBlockRaycast = extenData.isBlockRaycast;
                    m_operationParamStr = extenData.operationParam;
                    if (m_operationParamStr == null)
                    {
                        m_operationParamStr = "";
                    }
                    m_dialogRect = extenData.dialogRect;

                    m_dialogType = extenData.dialogType;
                    m_TextModuleId = extenData.TextModuleId;
                    if (m_TextModuleId==null)
                    {
                        m_TextModuleId = "";
                    }
                    m_FrameType = extenData.FrameType;
                    m_SkipButtonType = extenData.SkipButtonType;
                }
            }

        }

        public override void DrawNode(int id)
        {
            if (Data.ExtensionData != null)
            {
                if (Data.ExtensionData.GetType() == typeof(GuideStepExtensionData))
                {
                    m_EnvSelectedIndex = EditorGUILayout.Popup("环境选择", m_EnvSelectedIndex, GuideStepExtensionData.GetEnvPrefabs().ToArray());
                    m_OperationTypeSelectedIndex = EditorGUILayout.Popup("操作类型选择", m_OperationTypeSelectedIndex, GuideStepExtensionData.GetOperationTypes().ToArray());
                    DrawOperation(m_OperationTypeSelectedIndex);
                    m_maskType = (GuideStepMaskType)EditorGUILayout.EnumPopup(new GUIContent("遮罩类型"), m_maskType);
                    m_isTransparent = EditorGUILayout.Toggle(new GUIContent("是否透明"), m_isTransparent);
                    m_isBlockRaycast = EditorGUILayout.Toggle(new GUIContent("是否屏蔽点击事件"), m_isBlockRaycast);
                    m_dialogRect = EditorGUILayout.RectField("对话框区域", m_dialogRect);
                    m_dialogType=(GuideStepDialogType)EditorGUILayout.EnumPopup(new GUIContent("对话框类型"), m_dialogType);
                    EditorGUILayout.LabelField("对话框内容字段id");
                    m_TextModuleId = EditorGUILayout.TextField(m_TextModuleId);

                    m_FrameType = (GuideStepFrameType)EditorGUILayout.EnumPopup(new GUIContent("框选类型"), m_FrameType);
                    m_SkipButtonType= (GuideStepSkipButtonType)EditorGUILayout.EnumPopup(new GUIContent("跳过按钮类型"), m_SkipButtonType);
                }
            }
            base.DrawNode(id);


        }
        private void DrawOperation(int selectedIndex)
        {
            string operationType = GuideStepExtensionData.GetOperationTypes()[selectedIndex];
            switch (operationType)
            {
                case "Click":
                    {
                        m_ButtonNameSelectedIndex = EditorGUILayout.Popup("按钮名称选择", m_ButtonNameSelectedIndex, GuideStepExtensionData.GetButtonNames().ToArray());
                        
                        m_operationParamStr = GuideStepExtensionData.GetButtonNames()[m_ButtonNameSelectedIndex];
                    }

                    break;
                default:
                    {
                        
                        m_operationParamStr = EditorGUILayout.TextField(m_operationParamStr);


                    }
                    break;
            }
        }

        public override void WillSaveData(BaseNodeWindow baseNodeWindow)
        {
            base.WillSaveData(baseNodeWindow);
            Debug.Log("即将保存数据");
            GuideNodeWindow guideWindow = baseNodeWindow as GuideNodeWindow;
            SaveExtensionData(guideWindow);
        }
        private void SaveExtensionData(GuideNodeWindow guideWindow)
        {
            Data.ExtensionData = ScriptableObject.CreateInstance<GuideStepExtensionData>();

            GuideStepExtensionData extenData = Data.ExtensionData as GuideStepExtensionData;
            extenData.belongGuideId = guideWindow.GetGuideId();
            extenData.stepIndex = guideWindow.GetStepIndex(Data.Id);

            extenData.dialogRect = m_dialogRect;
            extenData.environmentPrefab = GuideStepExtensionData.GetEnvPrefabs()[m_EnvSelectedIndex];
            extenData.operationType = GuideStepExtensionData.GetOperationTypes()[m_OperationTypeSelectedIndex];
            extenData.operationParam = m_operationParamStr;
            extenData.maskType = m_maskType;
            extenData.isTransparent = m_isTransparent;
            extenData.isBlockRaycast = m_isBlockRaycast;

            extenData.dialogType = m_dialogType;
            extenData.TextModuleId = m_TextModuleId;
            extenData.FrameType = m_FrameType;
            extenData.SkipButtonType = m_SkipButtonType;

        }

        
    }
}