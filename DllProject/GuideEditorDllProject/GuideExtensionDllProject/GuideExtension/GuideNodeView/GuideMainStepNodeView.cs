﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using SimpleNodeBasedEditor;

namespace Guide.Editor
{
    public class GuideMainStepCommentEditorWindow : EditorWindow
    {

        private string nameText = "";
        private Action<string> onClick;
        public void Init(Action<string> m_callback, string title)
        {
            onClick = m_callback;
            titleContent.text = title;
        }
        private void OnGUI()
        {

            GUILayout.Label("请输入");
            nameText = GUILayout.TextField(nameText);
            if (GUILayout.Button("确定") && !string.IsNullOrEmpty(nameText))
            {
                Close();
                if (null != onClick)
                {
                    onClick(nameText);
                }

            }
            this.Focus();
        }
    }
    [BaseNode(false, "大步骤主节点")]
    public class GuideMainStepNodeView : BaseNodeView
    {

        #region UI Elements Fields
        private int m_guideId;
        private bool m_canSkip;
        private int m_keyStep;
        private bool m_hasNextStep;
        private int m_nextGuideId;
        private int m_nextGuideStepId = 1;
        private bool m_trustClient;
        private int m_triggerPrefabIndex;
        private string m_comment;


        #endregion

        public override void CreateData(int id, string type)
        {
            base.CreateData(id, type);
            Data.windowRect = new Rect(50 + 150 * (id - 1), 50, 250, 250);
            Data.NodeName = "Guide主节点";
            Data.ExtensionData = ScriptableObject.CreateInstance<GuideMainStepExtensionData>();
            GuideMainStepExtensionData extenData = Data.ExtensionData as GuideMainStepExtensionData;
            if (string.IsNullOrEmpty(m_comment))
            {
                m_comment = "请输入注释";
            }
        }

        public override void LoadData(BaseNodeData baseData)
        {
            base.LoadData(baseData);
            if (Data.ExtensionData != null)
            {
                if (Data.ExtensionData.GetType() == typeof(GuideMainStepExtensionData))
                {
                    GuideMainStepExtensionData extenData = Data.ExtensionData as GuideMainStepExtensionData;
                    m_guideId = extenData.guideId;
                    m_canSkip = extenData.canSkip;
                    m_keyStep = extenData.keyStep;
                    m_hasNextStep = extenData.hasNextStep;
                    m_nextGuideId = extenData.nextGuideId;
                    m_nextGuideStepId = extenData.nextGuideStepId;
                    m_trustClient = extenData.trustClient;
                    m_triggerPrefabIndex = GuideMainStepExtensionData.GetAllTriggers().IndexOf(extenData.guideTriggerPrefab);
                    if (m_triggerPrefabIndex==-1)
                    {
                        EditorUtility.DisplayDialog("所打开的窗口数据破损!", "请设置EnumDefine后重新打开窗口", "确定");
                    }
                    m_comment = extenData.commnet;

                    if (string.IsNullOrEmpty(m_comment))
                    {
                        m_comment = "请输入注释";
                    }

                }
            }
        }
        public override void DrawNode(int id)
        {

            GUILayout.Label("Guide主节点——整个窗口中只能存在一个");
            m_guideId = EditorGUILayout.IntField(new GUIContent("GuideId"), m_guideId);
            m_canSkip = EditorGUILayout.Toggle("能否跳过", m_canSkip);
            m_keyStep = EditorGUILayout.IntField(new GUIContent("关键步骤Id"), m_keyStep);
            m_hasNextStep = EditorGUILayout.Toggle(new GUIContent("在结束后是否立刻开始下一个引导", "这代表会立刻开始下一个步骤的引导而不是等待触发器触发"), m_hasNextStep);
            if (m_hasNextStep)
            {
                m_nextGuideId = EditorGUILayout.IntField(new GUIContent("->跳转的引导Id"), m_nextGuideId);
                m_nextGuideStepId = EditorGUILayout.IntField(new GUIContent("->跳转的步骤Id"), m_nextGuideStepId);
            }

            m_trustClient = EditorGUILayout.Toggle("是否信任客户端", m_trustClient);

            m_triggerPrefabIndex = EditorGUILayout.Popup("选择步骤触发器类型", m_triggerPrefabIndex, GuideMainStepExtensionData.GetAllTriggers().ToArray());
            m_comment = GUILayout.TextArea(m_comment);


            GUILayout.Space(70);
            if (GUILayout.Button("删除节点"))
            {
                DeleteNode();
            }
            NodeEditorEventManager.Instance.ProcessNodeEvent(Event.current, id);
            //设置改窗口可以拖动
            GUI.DragWindow();
        }

        public override void WillSaveData(BaseNodeWindow baseNodeWindow)
        {
            base.WillSaveData(baseNodeWindow);
            SaveExtensionData();
        }

        private void SaveExtensionData()
        {
            Data.ExtensionData = ScriptableObject.CreateInstance<GuideMainStepExtensionData>();
            GuideMainStepExtensionData extenData = Data.ExtensionData as GuideMainStepExtensionData;

            extenData.guideId = m_guideId;
            extenData.canSkip = m_canSkip;
            extenData.keyStep = m_keyStep;
            extenData.hasNextStep = m_hasNextStep;
            extenData.nextGuideId = m_nextGuideId;
            extenData.nextGuideStepId = m_nextGuideStepId;
            extenData.trustClient = m_trustClient;
            extenData.guideTriggerPrefab = GuideMainStepExtensionData.GetAllTriggers()[m_triggerPrefabIndex];

            extenData.commnet = m_comment;
        }
    }

}