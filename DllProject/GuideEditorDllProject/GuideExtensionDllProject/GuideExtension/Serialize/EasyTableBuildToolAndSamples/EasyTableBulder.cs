﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
namespace Guide.Editor
{
    public abstract class EasyTableBulder
    {
        public abstract string TableFullName { get; }

        protected StringBuilder curBuilder;
        public abstract string TableHeader { get; }
        public abstract string Commnet { get; }
        public abstract string FieldsNames { get; }
        public abstract string FieldsTypes { get; }
        public abstract string PrimaryTypes { get; }
        public abstract string ServerFields { get; }
        public virtual void BuildHead()
        {
            curBuilder = new StringBuilder();
            curBuilder.AppendLine(TableHeader);
            curBuilder.AppendLine(Commnet);
            curBuilder.AppendLine(FieldsNames);
            curBuilder.AppendLine(FieldsTypes);
            curBuilder.AppendLine(PrimaryTypes);
            curBuilder.AppendLine(ServerFields);

        }
        public abstract void BuildRow(object rowData);
        public virtual string GetString()
        {
            return curBuilder.ToString();

        }
    }
}