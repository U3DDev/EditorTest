﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Guide.Editor.Serialize;
namespace Guide.Editor
{

    public class GuideTableRow
    {
        public string guideId;
        public string canSkip;
        public string keyStep;
        public string hasNextGuide;
        public string nextGuideId;
        public string nextGuideStepId;
        public string trustClient;

        public string guideTriggerPrefab;

    }

    /// <summary>
    /// 负责生成Guide和GuideExtra表
    /// </summary>
    public class GuideTableExcelWriter
    {
        private List<TableHead> m_GuideTableHeadList = new List<TableHead>();
        private SerializeWriter m_GuideTableWriter = new SerializeWriter();

        private List<TableHead> m_GuideExtraTableHeadList = new List<TableHead>();
        private SerializeWriter m_GuideExtraTableWriter = new SerializeWriter();

        private int m_rowCount = 0;
        private bool m_hasAdded = false;
        public GuideTableExcelWriter()
        {
            //初始化Guide表头
            m_GuideTableHeadList.Add(new TableHead("大步骤", "guideId", "int", "primary", "guideId"));
            m_GuideTableHeadList.Add(new TableHead("是否可以跳过", "canSkip", "int", "none", "canSkip"));
            m_GuideTableHeadList.Add(new TableHead("关键步骤", "keyStep", "int", "none", ""));
            m_GuideTableHeadList.Add(new TableHead("是否有引导接续", "hasNextGuide", "int", "none", ""));
            m_GuideTableHeadList.Add(new TableHead("接续的引导大步骤", "nextGuideId", "int", "none", ""));
            m_GuideTableHeadList.Add(new TableHead("接续的引导小步骤", "nextGuideStepId", "int", "none", ""));
            m_GuideTableHeadList.Add(new TableHead("是否信任客户端", "trustClient", "int", "none", "trustClient"));

            //初始化Extra表头
            m_GuideExtraTableHeadList.Add(new TableHead("大步骤", "guideId", "int", "primary", "guideId"));
            m_GuideExtraTableHeadList.Add(new TableHead("触发预制id", "TriggerPrefabId", "string", "none", ""));

        }
        public void SetRowCount(int rowCouunt)
        {
            m_rowCount = rowCouunt;
        }
        public void AddTableHead()
        {
            if (m_hasAdded)
            {
                return;
            }
            m_GuideTableWriter.PrepareWrite("Guide.xlsx", m_rowCount, 7);
            m_GuideExtraTableWriter.PrepareWrite("GuideExtra.xlsx", m_rowCount, 2);

            foreach (var head in m_GuideTableHeadList)
            {
                m_GuideTableWriter.AddTableHead(head);
            }
            foreach (var head in m_GuideExtraTableHeadList)
            {
                m_GuideExtraTableWriter.AddTableHead(head);
            }
            m_hasAdded = true;
        }
        public void AddRow(int index, GuideTableRow rowData)
        {
            if (null != rowData)
            {
                m_GuideTableWriter.AddTableData(index, rowData.guideId, rowData.canSkip, rowData.keyStep, rowData.hasNextGuide, rowData.nextGuideId, rowData.nextGuideStepId, rowData.trustClient);
                m_GuideExtraTableWriter.AddTableData(index, rowData.guideId,rowData.guideTriggerPrefab);

            }

        }

        public void SaveToExcel(string fullPath)
        {
            m_GuideTableWriter.WriteToExcel(fullPath+"/Guide.xlsx");
            m_GuideExtraTableWriter.WriteToExcel(fullPath+"/GuideExtra.xlsx");
        }

    }
}


