﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SimpleNodeBasedEditor;
using UnityEngine;
using UnityEditor;
namespace Guide.Editor
{
    [BaseNodeWindow(false, "新手引导窗口")]
    public class GuideNodeWindow : BaseNodeWindow
    {

        private string excelExportPath;
        public override void LoadData(int PageId)
        {
            base.LoadData(PageId);
            if (WindowData.ExtensionObject != null)
            {
                if (WindowData.ExtensionObject.GetType() == typeof(GuideNodeWindowExtensionData))
                {
                    GuideNodeWindowExtensionData extensionData = WindowData.ExtensionObject as GuideNodeWindowExtensionData;
                    excelExportPath = extensionData.excelSavePath;
                    if (string.IsNullOrEmpty(excelExportPath))
                    {
                        excelExportPath = "";
                    }
                }

            }
        }
        public override void CreateData(int pageId, string type)
        {
            base.CreateData(pageId, type);
            WindowData.ExtensionObject = ScriptableObject.CreateInstance<GuideNodeWindowExtensionData>();
        }
        public override void SaveToData()
        {
            base.SaveToData();
            WindowData.ExtensionObject = ScriptableObject.CreateInstance<GuideNodeWindowExtensionData>();
            GuideNodeWindowExtensionData extensionData = WindowData.ExtensionObject as GuideNodeWindowExtensionData;
            extensionData.excelSavePath = excelExportPath;
        }

        public override void DrawWindow()
        {
            base.DrawWindow();
            GUI.Label(new Rect(5, 110, 200, 15), "这是一个引导窗口");
            //GUI.Label(new Rect(5, 110, 200, 15), excelExportPath);
            //if (GUI.Button(new Rect(210, 110, 200, 15), "选择excel导出路径"))
            //{

            //    excelExportPath = EditorUtility.OpenFolderPanel("选择保存的文件夹", Application.dataPath, "excelFolder");
            //    excelExportPath = excelExportPath.Substring(excelExportPath.IndexOf("Assets/") + 7);
            //}
            //if (GUI.Button(new Rect(410, 110, 100, 15), "ExportData"))
            //{
            //    if (string.IsNullOrEmpty(excelExportPath))
            //    {
            //        EditorUtility.DisplayDialog("导出失败", "请选择导出路径", "确定");

            //    }
            //    else
            //    {
            //        if (EditorUtility.DisplayDialog("将会导出数据到excel", "这将会覆盖原先的数据", "确定", "取消"))
            //        {
            //            //TODO 

            //        }
            //    }

            //}
        }
        public override BaseNodeView CreateNewNode(Type type = null)
        {
            if (type == typeof(GuideMainStepNodeView))
            {
                if (!CheckExistMainNode())
                {
                    return base.CreateNewNode(type);
                }
            }
            else
            {
                return base.CreateNewNode(type);

            }
            EditorUtility.DisplayDialog("创建错误", "一个引导只能存在一个主节点", "确定");
            return null;
        }

        private bool CheckExistMainNode()
        {
            foreach (var nodeView in m_NodeViewDict.Values)
            {
                if (nodeView.GetType() == typeof(GuideMainStepNodeView))
                {
                    return true;
                }
            }
            return false;
        }

        public int GetGuideId()
        {
            //Search MainStep

            var mainStep = GetMainStepNodeView();
            if (null != mainStep)
            {
                return (mainStep.Data.ExtensionData as GuideMainStepExtensionData).guideId;
            }

            return -9999;
        }

        public int GetStepIndex(int id)
        {
            var mainStep = GetMainStepNodeView();            
            if (null != mainStep)
            {
                int searchedIndex = 1;
                var node = GetNode(mainStep.Data.LinkId);
                while (node != null)
                {
                    if (node.Data.Id==id)
                    {
                        return searchedIndex;
                    }
                    node = GetNode(node.Data.LinkId);
                    searchedIndex++;

                }


            }


            return -9999;
        }


        public GuideMainStepNodeView GetMainStepNodeView()
        {
            foreach (var nodeView in m_NodeViewDict.Values)
            {
                if (nodeView.Data.NodeViewType == typeof(GuideMainStepNodeView).FullName)
                {
                    return nodeView as GuideMainStepNodeView;
                }
            }

            return null;
        }
    }

}