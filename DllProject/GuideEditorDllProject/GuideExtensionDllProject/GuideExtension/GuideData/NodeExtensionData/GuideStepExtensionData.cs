﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
namespace Guide.Editor
{

    [Serializable]
    public class GuideStepExtensionData : ScriptableObject
    {
        public int belongGuideId;
        public int stepIndex;

        public Rect dialogRect;
        public string environmentPrefab;
        public string operationType;
        public string operationParam;
        /// <summary>
        /// 0 点击区域屏蔽 1全屏蔽例如一个tip
        /// </summary>
        public GuideStepMaskType maskType = 0;
        public GuideStepDialogType dialogType = 0;
        public bool isTransparent = false;
        public bool isBlockRaycast = true;
        public string TextModuleId;
        public GuideStepFrameType FrameType = 0;
        public GuideStepSkipButtonType SkipButtonType = 0;


        public static List<string> GetEnvPrefabs()
        {
            if (null!=GuideMainEditorWindow.EnumDefineData)
            {
                return GuideMainEditorWindow.EnumDefineData.EnvPrefabs;
            }
            return new List<string>() { "PleaseGenEnum First" };
        }
        public static List<string> GetOperationTypes()
        {
            return new List<string>() { "Click", "WaitForFlagChange", "OpenPage", "PathFinding", "SkillGuide" };
        }

        public static List<string> GetButtonNames()
        {
            if (null != GuideMainEditorWindow.EnumDefineData)
            {
                return GuideMainEditorWindow.EnumDefineData.Buttons;
            }
            return new List<string>() { "PleaseGenEnum First" };
        }
    }

    [Serializable]
    public enum GuideStepMaskType : int
    {
        AreaLight = 0,
        AllLight = 1
    }
    [Serializable]
    public enum GuideStepDialogType : int
    {
        Hidden = 0,
        ShowWithoutPointer = 1
    }

    /// <summary>
    /// 0：不显示外框
    //  1：不显示指引的外框
    //  2：左侧指引的外框
    //  3：右侧指引的外框
    /// </summary>
    [Serializable]
    public enum GuideStepFrameType : int
    {
        Hidden = 0,
        ShowWithFrameOnly = 1,
        ShowWithLeftPointer = 2,
        ShowWithRightPointer = 3,
    }
    [Serializable]
    public enum GuideStepSkipButtonType : int
    {
        Hidden = 0,
        TopLeft = 1,
        BottomLeft=2
    }
}