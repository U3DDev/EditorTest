﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Guide.Editor
{
    public class GuideMainStepExtensionData : ScriptableObject
    {
        public int guideId;
        public bool canSkip = false;
        public int keyStep;
        public bool hasNextStep;
        public int nextGuideId;
        public int nextGuideStepId;
        public bool trustClient;

        //GuideExtra
        public string guideTriggerPrefab;

        public string commnet;

        public static List<string> GetAllTriggers()
        {

            if (null != GuideMainEditorWindow.EnumDefineData)
            {
                return GuideMainEditorWindow.EnumDefineData.Triggers;
            }
            return new List<string>() { "PleaseGenEnum First" };
        }
    }

}