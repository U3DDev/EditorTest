﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
namespace Guide.Editor
{

    [CreateAssetMenu(fileName = "EnumDefineData", menuName = "引导数据/创建枚举数据")]
    public class EnumDefineScriptableObject:ScriptableObject
    {
        public List<string> EnvPrefabs=new List<string>();
        public List<string> Buttons=new List<string>();

        public List<string> Triggers=new List<string>();
    }
}
